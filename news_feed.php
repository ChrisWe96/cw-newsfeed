<?php
require_once __DIR__ . '/vendor/autoload.php';
include './php/facebookGraphConsumer/GraphClient.php';

use BitzingerFacebookClient\GraphClient;

const DOT_ENV_PATH = __DIR__;

loadEnvironmentVariables(); // loading secrets from a .env file
displayNewsfeed();

/**
 * Creates a newsfeed, with multiple cards.
 * @return void
 */
function displayNewsfeed($articleLimit = 6)
{
    $posts = getPosts(10);

    echo '<section class="container"><section id="newsfeed" class="row">';

    $i = 0;
    foreach ($posts as $post) {
        if ($i++ < $articleLimit) {
            displayNewsFeedCard($post->full_picture ?? 'no img', $post->message ?? 'no title');
        }

    }

    echo '</section></section>';
}


/**
 * Displays a news feed item card.
 *
 * @param $imgUrl  string the path to the background image.
 * @param $showDate  string display property for date.
 * @param $showDescription string  display property for description.
 * @return void
 */
function displayNewsFeedCard($imgUrl, $title = 'Heading', $showDate = true, $showDescription = false)
{
    echo '<article class="col-sm-12 col-md-6 col-lg-4 mb-4">
            <div class="card text-white card-has-bg" style="background-image:url(' . $imgUrl . ');">
                <div class="card-img-overlay d-flex flex-column">
                    <div class="card-body"></div>
                    <div class="card-footer">';

    echo '<small class="card-meta mb-2">' . $title . '</small><br>';

    if ($showDescription) {
        echo '<h5 class="card-title mt-0 "><a class="text-white" href="#">Text Description</a></h5>';
    }

    if ($showDate) {
        echo '<small><i class="far fa-clock"></i> October 15, 2020</small>';
    }

    echo '</div></div></div></article>';
}


function getPosts($id)
{
    $client = new GraphClient();
    return $client->getPosts($id);
}

/**
 * loads the .env in root directory and makes its vars available in $_ENV.
 * @return void
 */
function loadEnvironmentVariables(): void
{
    (Dotenv\Dotenv::createUnsafeImmutable(DOT_ENV_PATH))->load();
}