window.onload = () => {
    updateSelectList().then();
};

async function updateSelectList() {
    const select = document.getElementById('feedSelect');

    select.innerHTML ='';
    const optionsList = document.createDocumentFragment();

    await fetch("php/getNewsFeeds.php")
        .then((res) => res.json())
        .then((newsFeeds) => {
            newsFeeds.map(nf => {
                let option = document.createElement('option');
                document.createElement('option')
                option.text = nf.news_feed_title;
                option.value = nf.uid;
                optionsList.append(option);
            });

            console.log(optionsList);

        })
        .catch((error) => console.log(error));

    if(!optionsList.hasChildNodes()){
        let defaultOption = document.createElement('option')
        defaultOption.text =  "Choose..."
        select.appendChild(defaultOption);
    }

    select.appendChild(optionsList);

    console.log(document.getElementById('feedSelect'));
}

