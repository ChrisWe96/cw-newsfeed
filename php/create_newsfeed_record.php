<?php
include "db_connection.php";

if(getParametersValid()){
    DB_CON::executeDBStatement(createSQLStatement());
}


function getParametersValid(): bool
{
    return isset($_GET['newsFeedId'])
        && isset($_GET['description'])
        && isset($_GET['title'])
        && isset($_GET['imageUrl']);
}

function createSQLStatement(): string
{
    return "INSERT INTO news_feed_article (news_feed_id, image_url, title, description) 
            VALUES ('"
        . $_GET['newsFeedId'] . "','"
        . $_GET['imageUrl'] . "', '"
        . $_GET['title'] . "', '"
        . $_GET['description'] . "')";
}



