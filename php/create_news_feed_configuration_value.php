<?php
include "db_connection.php";

if (getParametersValid()) {
    DB_CON::executeDBStatement(createSQLStatement());
}


function getParametersValid(): bool
{
    return isset($_GET['newsFeedId'])
        && isset($_GET['key'])
        && isset($_GET['value']);
}

function createSQLStatement(): string
{
    return "INSERT INTO news_feed_configuration_values (news_feed_id, config_key, config_value) 
        VALUES ('"
        . $_GET['newsFeedId'] . "','"
        . $_GET['key'] . "', '"
        . $_GET['value'] . "')";
}