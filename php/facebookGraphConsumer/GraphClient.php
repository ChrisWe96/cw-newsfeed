<?php

namespace BitzingerFacebookClient;

require_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/../db_connection.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use DB_CON;

/**
 * Class for creating a FaceBook client, that can pull posts from a profile.
 */
class GraphClient
{
    private const RESULT_LIMIT = 25;
    private string $base_url = "https://graph.facebook.com/";
    private Client $client;

    /**
     * Build a client with a configurable api version.
     * @param string $api_version
     */
    public function __construct(string $api_version = "v15.0/")
    {
        $this->base_url .= $api_version;
        $this->client = new Client([
            'base_uri' => $this->base_url
        ]);
    }

    /**
     * Gets all post of a newsfeed, if there's a corresponding access_token stored in the db.
     * @param $newsFeedID
     * @return void
     */
    public function getPosts($newsFeedID)
    {
        $access_token = $this->getNewsFeedAccessToken($newsFeedID);
        $token = $access_token["access_token"];
        try {
            $response = $this->client->get('me/posts/', [
                'query' => [
                    'fields' => 'created_time,full_picture,id,message,type,permalink_url',
                    'limit' => self::RESULT_LIMIT,
                    'access_token' => $token
                ]
            ]);
        } catch (GuzzleException $e) {
            die("Client creation failed: " . $e->getMessage());
        }
        $content = $response->getBody()->getContents();
        $jsonContent = json_decode($content);
        return $jsonContent->data;
    }

    /**
     * Retrieves a long-lived access_token, valid for 60 days, by using a regular short-lived access_token.
     * @param $accessToken string
     * @return void
     */
    public function getLongLiveAccessToken($accessToken)
    {
        $var = $_ENV['APP_SECRET'];
        try {
            $response = $this->client->get('oauth/access_token', [
                'query' => [
                    'grant_type' => 'fb_exchange_token',
                    'client_id' => $_ENV['APP_SECRET'],
                    'client_secret' => $_ENV['APP_ID'],
                    'fb_exchange_token' => $accessToken
                ]
            ]);
        } catch (GuzzleException $e) {
            die("Client creation failed: " . $e->getMessage());
        }
        $content = $response->getBody()->getContents();
        $jsonContent = json_decode($content);
        return $jsonContent->access_token;

    }

    /**
     * Gets the corresponding access_token of a newsfeed from the database.
     * @param $newsFeedID int
     * @return array|false|null
     */
    private function getNewsFeedAccessToken(int $newsFeedID): bool|array|null
    {
        return DB_CON::getNewsFeedAccessToken($newsFeedID);
    }
}