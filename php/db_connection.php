<?php

class DB_CON
{
    private const SERVER = "ddev-cw-newsfeed-db";
    private const USER = "root";
    private const PASSWORD = "root";
    private const DATABASE = "cw_newsfeed";

    public static function getDBConnection(): mysqli
    {
        return new mysqli(self::SERVER, self::USER, self::PASSWORD, self::DATABASE);
    }

    public static function executeDBStatement($sql)
    {
        $conn = self::getDBConnection();

        // Check connection
        self::checkConnection($conn);

        // Execute SQL Statement
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            $error = $conn->error;
            echo "Error: " . $sql . "<br>" . $error;
        }


        $conn->close();
        header("Location: https://cw-newsfeed.ddev.site");
        die();
    }

    public static function getFromDB($sql)
    {
        $conn = self::getDBConnection();

        self::checkConnection($conn);

        // Execute SQL Statement
        $result = $conn->query($sql);
        $rows = array();

        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }

        $conn->close();
        return json_encode($rows);
    }

    public static function getNewsFeedAccessToken($newsFeedID)
    {
        $conn = self::getDBConnection();

        self::checkConnection($conn);

        $sql = 'SELECT access_token FROM news_feed WHERE uid =' . $newsFeedID;
        $result = $conn->query($sql);

        $conn->close();
        return mysqli_fetch_assoc($result);

    }

    public static function checkConnection($connection)
    {
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }
}
