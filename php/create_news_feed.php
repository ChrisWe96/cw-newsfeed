<?php
include 'db_connection.php';
include  'facebookGraphConsumer/GraphClient.php';

use BitzingerFacebookClient\GraphClient;



if(getParametersValid()){
    $longLiveAccessToken = getLongLiveAccessToken();
    DB_CON::executeDBStatement(createSQLStatement($longLiveAccessToken));
}


/**
 * Checks if title and access_token are in the URL
 * @return bool true if both params are in the url else false.
 */
function getParametersValid(): bool
{
    return isset($_GET['newsFeedTitle']) && isset($_GET['accessToken']);
}


/**
 * Builds a SQL query for saving the newsfeed and the access_token.
 * @param $longLiveAccessToken
 * @return string
 */
function createSQLStatement($longLiveAccessToken): string
{
    return "INSERT INTO news_feed (news_feed_title, access_token) 
            VALUES ('" . $_GET['newsFeedTitle'] . "', '" . $longLiveAccessToken . "')";
}

/**
 * Retrieves the long-lived version of an access token.
 * @return null|string
 */
function getLongLiveAccessToken()
{
    $client = new GraphClient();
    return  $client->getLongLiveAccessToken($_GET['accessToken']);
}