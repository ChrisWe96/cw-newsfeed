<!doctype html>
<html lang="de">
<head>
    <title>Chris's Newsfeed</title>

    <link rel="stylesheet" href="newsfeed-card.css">
    <!--  Font Awesome Icons  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
          integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <!--  Bootstrap 5 Resources  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
            integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>

<body>

<header class="container-fluid p-5 mb-3 bg-secondary text-white text-center">
    <h1>Chris's News Feed</h1>


    <form>
        <div class="form-group">
            <label for="newsFeedTitle">Choose Newsfeed:</label>
            <select class="custom-select" id="feedSelect" >
                <option selected>Choose...</option>
            </select>
        </div>
    </form>


</header>

<?php include 'news_feed.php'; ?>

<!-- Create News Feed  -->
<section class="container">
    <hr>
    <h3>Create News Feed</h3>

    <form action="php/create_news_feed.php" method="get">
        <div class="form-group mt-3">
            <label for="newsFeedTitle">News Feed Title</label>
            <input type="text" class="form-control mt-1" placeholder="Enter News Feed Title" name="newsFeedTitle"
                   required>
        </div>
        <div class="form-group mt-3">
            <label for="accessToken">Access Token</label>
            <input type="text"
                   class="form-control mt-1" placeholder="Enter Access Token" name="accessToken"
                   required>
        </div>
        <button type="submit" class="btn btn-primary mt-3 mb-5">Create Newsfeed</button>
    </form>
</section>

<!-- Form News Feed Article -->
<section class="container">
    <hr>
    <h3>Create News Feed Article</h3>

    <form action="php/create_newsfeed_record.php" method="get">
        <div class="form-group mt-3">
            <label for="newsFeedId">News Feed (ID)</label>
            <input type="text" class="form-control mt-1" placeholder="Select Newsfeed" name="newsFeedId"
                   required>
        </div>
        <div class="form-group mt-3">
            <label for="imageUrl">Image URL</label>
            <input type="text" class="form-control mt-1" placeholder="Enter Image URL" name="imageUrl">
        </div>
        <div class="form-group mt-3">
            <label for="title">Title</label>
            <input type="text" class="form-control mt-1" placeholder="Enter News Title" name="title"
                   required>
        </div>
        <div class="form-group mt-3">
            <label for="description">Description</label>
            <input type="text" class="form-control mt-1" placeholder="Enter News Description"
                   name="description" required>
        </div>
        <button type="submit" class="btn btn-primary mt-3 mb-5">Add News Item</button>
    </form>
</section>

<!-- Create Newsfeed Config Values  -->
<section class="container">
    <hr>
    <h3>Create News Feed Config Values</h3>

    <form action="php/create_news_feed_configuration_value.php" method="get">
        <div class="form-group mt-3">
            <label for="newsFeedId">News Feed</label>
            <input type="text" class="form-control mt-1" placeholder="Select Newsfeed" name="newsFeedId"
                   required>
        </div>

        <div class="row">
            <div class="col form-group mt-3">
                <label for="key">Key</label>
                <input type="text" class="form-control mt-1" placeholder="Enter Config Key" name="key"
                       required>
            </div>
            <div class="col form-group mt-3">
                <label for="value">Value</label>
                <input type="text" class="form-control mt-1" placeholder="Enter Config Value" name="value"
                       required>
            </div>
        </div>


        <button type="submit" class="btn btn-primary mt-3 mb-5">Create Newsfeed</button>
    </form>
</section>

<script src="index.js"></script>
</body>
</html>